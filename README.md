# Device Microservices Installer Description

## Description

This project is designed to install and run your microservices in easy steps.

This project has dependencies on: 
- [Device Configuration Service](https://gitlab.com/evgenivanovi/microservice-configuration)
- [Device Registry Service](https://gitlab.com/evgenivanovi/microservice-registry)

## How to install:

All applications should start using `Docker Compose`. 
File `docker-compose.yml` contains three images: a micro service Device Configuration, the micro service Device Registry, and Mongo Database.

#### Installation steps
- Make sure that the dependencies are downloaded and stored in the local repository.
- Run the `mvn clean package` command. This command will prepare the artifact `Device-Microservices-Installer-${version}.tar.gz`.
- Unpack the archive to a convenient directory and then in the root directory call the command `docker-compose up --build -V`.

---

## Configuration

#### Customize configuration using environment variables

When you start docker-compose, you can adjust the initialization of the docker containers by passing (changing) one or more environment variables in the `.env` file. 

- `CONFIGURATION_SERVICE_PORT` - Describes on which port Configuration Service should be accessible.
- `REGISTRY_SERVICE_PORT` - Describes on which port Registry Service should be accessible.
- `MONGO_DB_PORT` - Describes on which port Mongo DB should be accessible.

